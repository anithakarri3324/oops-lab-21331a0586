/* Demonstrate the following :
1. Private Inheritance
2. Protected Inheritance
3. Public Inheritance */

#include <iostream>
using namespace std;

// Base class
class Animal {
public:
    void eat() {
        cout << "I can eat!" << endl;
    }

protected:
    void sleep() {
        cout << "I can sleep!" << endl;
    }

private:
    void breathe() {
        cout << "I can breathe!" << endl;
    }
};

// Derived class using private inheritance
class Dog1 : private Animal {
public:
    void function() {
        eat();
        sleep();
        // breathe(); // This won't work because breathe() is private in Animal
    }
};

// Derived class using protected inheritance
class Dog2 : protected Animal {
public:
    void function() {
        eat();
        sleep();
        // breathe(); // This won't work because breathe() is private in Animal
    }
};

// Derived class using public inheritance
class Dog3 : public Animal {
public:
    void function() {
        eat();
        sleep();
        // breathe(); // This won't work because breathe() is private in Animal
    }
};

int main() {
    Dog1 obj1;
    // obj1.breathe(); // This won't work because breathe() is private in Dog1
    obj1.function(); // This will call eat() and sleep()

    Dog2 obj2;
    // obj2.breathe(); // This won't work because breathe() is private in Cat2
    // obj2.sleep(); // This won't work because sleep() is protected in Cat2
    obj2.function(); // This will call eat() and sleep()

    Dog3 obj3;
    // obj3.breathe(); // This won't work because breathe() is private in Cat3
    // obj3.sleep(); // This won't work because sleep() is protected in Cat3
    obj3.function(); // This will call eat() and sleep()

    return 0;
}
