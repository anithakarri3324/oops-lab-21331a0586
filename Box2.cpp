#include<iostream>
using namespace std;
class Box{
    public:
    float l,w,h;
        void boxArea(){
            cout<< "The area is "<< ( 2*l*w + 2*l*h + 2*h*w )<<endl; 
        };

        void boxVolume();

        friend void displayBoxDimensions(Box& b);

        inline void displayWelcomeMessage(){
            cout<<"Welcome"<<endl;
        };
        
};

void displayBoxDimensions(Box& b){
            cout << "Length is " << b.l <<" Width is " << b.w << " Height is " << b.h << endl;
        };

    //Scope resolution operator//
void Box::boxVolume(){
        cout << "The volume is " << (l * w * h) << endl;
};

int main(){
    Box b;
    float l,w,h;
    cout << "Enter the length, width and height: ";
    cin >> b.l >> b.w >> b.h ;
    b.displayWelcomeMessage();
    displayBoxDimensions(b);
    b.boxArea();
    b.boxVolume();
}

