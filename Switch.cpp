#include <iostream>
using namespace std;
int main() {
    float num1, num2, result;
    char op;
            // Prompt the user to enter two numbers
    cout << "Enter first number: ";
    cin >> num1;
    cout << "Enter second number: ";
    cin >> num2;
            // Prompt the user to choose an operation
    cout << "Choose an operation (+, -, *, /): ";
    cin >> op;
        // Perform the selected operation
    switch (op) {
        case '+':
            result = num1 + num2;
            break;
        case '-':
            result = num1 - num2;
            break;
        case '*':
            result = num1 * num2;
            break;
        case '/':
            if (num2 != 0) {
                result = num1 / num2;
            } else {
                cout << "Error: division by zero" << endl;
                return 1;
            }
            break;
        default:
            cout << "Error: invalid operation" << endl;
            return 1;
    }
    // Display the result
    cout << num1 << " " << op << " " << num2 << " = " << result << endl;
}




