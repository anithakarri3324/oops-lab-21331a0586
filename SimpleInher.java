// Parent class
class Animal {
    public void makeSound() {
      System.out.println("The animal makes a sound");
    }
  }
  
  // Child class
  class Dog extends Animal {
    public void makeSound() {
      System.out.println("The dog barks");
    }
  }
  
  // Main class
  public class SimpleInher {
    public static void main(String[] args) {
      Animal myAnimal = new Animal();
      Dog myDog = new Dog();
  
      myAnimal.makeSound(); 
      myDog.makeSound(); 
    }
  }
  