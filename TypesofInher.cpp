#include <iostream>
using namespace std;

// Base Class for Single and Multiple Inheritance
class Animal {
public:
    void eat() {
        cout << "Animal is eating." << endl;
    }
};

// Derived Class using Single Inheritance
class Dog : public Animal {
public:
    void bark() {
        cout << "Dog is barking." << endl;
    }
};

// Derived Class using Multiple Inheritance
class FlyingAnimal {
public:
    void fly() {
        cout << "FlyingAnimal is flying." << endl;
    }
};

class Bird : public Animal, public FlyingAnimal {
public:
    void chirp() {
        cout << "Bird is chirping." << endl;
    }
};

// Derived Class using Multilevel Inheritance
class Mammal : public Animal {
public:
    void sleep() {
        cout << "Mammal is sleeping." << endl;
    }
};

class Bat : public Mammal, public FlyingAnimal {
public:
    void squeak() {
        cout << "Bat is squeaking." << endl;
    }
};

// Derived Class using Hierarchical Inheritance
class Cat : public Animal {
public:
    void meow() {
        cout << "Cat is meowing." << endl;
    }
};

class Lion : public Cat {
public:
    void roar() {
        cout << "Lion is roaring." << endl;
    }
};

// Derived Class using Hybrid Inheritance
class Fish {
public:
    void swim() {
        cout << "Fish is swimming." << endl;
    }
};

class Shark : public Fish, public Animal {
public:
    void bite() {
        cout << "Shark is biting." << endl;
    }
};

int main() {
    // Single Inheritance
    cout<<endl;
    cout<<"_______Single Inheritance_______"<<endl;
    Dog dog;
    dog.eat();
    dog.bark();

    // Multiple Inheritance
    cout<<endl;
    cout<<"_______Multiple Inheritance_______"<<endl;
    Bird bird;
    bird.eat();
    bird.fly();
    bird.chirp();

    // Multilevel Inheritance
    cout<<endl;
    cout<<"_______Multilevel Inheritance_______"<<endl;
    Bat bat;
    bat.eat();
    bat.sleep();
    bat.fly();
    bat.squeak();

    // Hierarchical Inheritance
    cout<<endl;
    cout<<"_______Hierarchical Inheritance_______"<<endl;
    Lion lion;
    lion.eat();
    lion.meow();
    lion.roar();

    // Hybrid Inheritance
    cout<<endl;
    cout<<"_______Hybrid Inheritance_______"<<endl;
    Shark shark;
    shark.eat();
    shark.swim();
    shark.bite();
    cout<<endl;

    return 0;
}
